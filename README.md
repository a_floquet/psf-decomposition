# Fast shift-variant convolution approximation 
## Principle
Sometimes in image processing (and other fields as well), we come across a degradation that cannot be modelled as a simple convolution, $y(r) = \int h(r-s)x(s)ds$, but rather as a *shift-variant* convolution, that is a convolution where the kernel $h$ does not only depend on the difference $r-s$, but also on the position on the image $s$. We have  $y(r) = \int h(r-s, s)x(s)ds$.
If evaluating this integral is too computationally expensive, we can approximate it. There are several way of doing so, described in [this paper](https://ujm.hal.science/ujm-00979825/document). 

We chose to implement the method proposed by [Flicker and Rigaut](https://opg.optica.org/josaa/abstract.cfm?uri=josaa-22-3-504). The idea is to evaluate $h$ at $N$ points, decompose the $h_n$ onto $K$ modes, $\tilde{h}_k$, obtained by SVD of $H$, where the n-th column of $H$ is $h_n$. We then have a representation of $h$ using $K$ coefficients, at $N$ points. By interpolation (and extrapolation) of those coefficients across the whole image, we estimate $h$ at all points. 
The convolution is then done by weighting the image $K$ times, with each weights $w_k$, then convolving it with the associated modes $\tilde{h}_k$, and finally summing the results.

![](principle.png)

 To be faster, we can ditch the modes associated with low singular values, so we have $K\leq N$.

## What you will find in this repository
The purpose of this repository is to be pedagogical. There are 3 scripts to be used : 

 1. $\verb+decompose_kernel.m+$ : This is meant to show you how the SVD is used to extract modes, on wich the kernels can be decomposed. Notice the link between the number of mode used, the approximation error and the singular values !
 2. $\verb+shift_variant_convolution.m+$ : Instead of evaluating $h$ at $N$ points, you interactively place $N$ pre-defined $h_n$ where you want, by clicking on the test image. Then the shift-variant convolution is computed and the result is shown. You also have to specify, by looking at the singular values, how many mode you want to use.
 3. $\verb+shift_variant_deconvolution.m+$ : Same as script 2 but there is a deconvolution step, using GlobalBioIm.

## Dependencies
[GlobalBioIm](https://arxiv.org/pdf/1812.07908) ([find doc here](https://biomedical-imaging-group.github.io/GlobalBioIm/index.html)) is used, but really only needed for the deconvolution part. If you just wish to experiment with shift-variant convolution, and prefer to work with native MatLab functions, use the $\verb+shiftVariantConv()+$ function (it will also plot the convolution steps).

$\verb+ccc;+$ is just an alias for $\verb+clear all; close all; clc;+$.
