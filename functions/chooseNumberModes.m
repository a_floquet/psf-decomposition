function k = chooseNumberModes()
% Create dialog box to choose a number of mode to be used
prompt   = {'Enter number of modes to be used'};
title    = '';
sz       = [1, 35];
definput = {'1'};
k = inputdlg(prompt, title, sz, definput);
k = str2num(cell2mat(k));
end

