function [A, E] = computeApprox(H, U, sz)
% Compute the approximation of kernels, using  iteratively the first k-th
% mode.
N = size(H,2);
A = zeros(prod(sz), N, N); 
E = zeros(N, N);
% Loop for each kernel
for n = 1:N
    norm_Hn = sum(H(:,n).^2);
    % Loop for each mode (cumulative)
    for k = 1:N
        a      = linsolve(U(:,1:k), H(:, n));
        approx = U(:, 1:k) * a; 
        error  = sum((approx - H(:,n)).^2)/norm_Hn; 
        
        A(:, n, k) = approx;
        E(n, k)    = error;
    end  
end
end

