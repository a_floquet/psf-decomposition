function W = computeWeights(idx, H, U, sz)
% Compute weights associates with each modes
K = size(U, 2);
W = zeros([sz K]);
A = linsolve(U, H);

% Interpolation & extrapolation between placed points for each mode weights
[X, Y] = meshgrid(1:sz);
for k = 1:K
    F = scatteredInterpolant(idx(:,1), idx(:,2), A(k,:)', 'natural');
    W(:,:,k) = F(X, Y); 
end
end

