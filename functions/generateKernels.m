function H = generateKernels(kernel_type, N, m, M, sz, len)
% Create and fill matrix of kernels
H     = zeros(prod(sz), N);
sigma = linspace(m, M, N);

for n = 1:N
    s = sigma(n);
    switch  kernel_type
        case 'gaussian'
            h = fspecial(kernel_type, sz, s);
        case 'motion'
            h = fspecial(kernel_type, len, s);
            h = padToSize(h, sz);
        case 'levin'
            path_n = strcat('../levin_kernels/kernel', num2str(n), '.png');
            h = double(imread(path_n));
            h = padToSize(h, sz);
        otherwise
            error('given kernel type not recognized')
    end
    H(:, n) = h(:);
end
end

