function [a, b] = getFigsize(N)
% Convert N number of subplots into best a cols and b rows.
a = ceil(sqrt(N));
b = round(sqrt(N));
end

