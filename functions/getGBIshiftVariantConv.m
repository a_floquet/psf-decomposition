function model = getGBIshiftVariantConv(U, weights, sz_x, sz_kernel)
% Get a GlobalBioIm model that computes shift variant convolution
% Compute padding and cropping requirements
K        = size(U,2);
kernels  = reshape(U, [sz_kernel K]);
convsize = sz_x + sz_kernel - 1;
pad_ker  = convsize - sz_kernel;
kerpad   = padarray(kernels, floor(pad_ker/2), 'pre');
kerpad   = padarray(kerpad , ceil(pad_ker/2) , 'post');
x_min    = floor(sz_kernel(1)/2) + 1;
x_max    = floor(sz_kernel(1)/2) + sz_x(1);
y_min    = floor(sz_kernel(2)/2) + 1;
y_max    = floor(sz_kernel(2)/2) + sz_x(1);

% Define operators
B        = LinOpBroadcast([sz_x, K], 3);
W        = LinOpDiag(size(weights), weights);
Crop     = LinOpSelectorPatch([convsize K], [x_min y_min 1], [x_max y_max K]);
Pad      = Crop.makeAdjoint();
H        = LinOpConv('PSF', kerpad, true, [1 2], 'Centered');
S        = LinOpSum([sz_x K], 3);

% Assemble operator to create the model
model    = S * Crop * H * Pad * W * B;
end
