function [xopt, C_values, endMessage] = optimWithVMLMB(C, groud_truth, init, ItUpOut, maxiter, m)
% Settings
OutOp   = OutputOptiSNR(1, groud_truth, 10);
CvOp    = TestCvgCombine('CostRelative', 1e-9, 'StepRelative', 1e-9); 

% Instantiate opt and run it
optimizer         = OptiVMLMB(C, 0, []); 
optimizer.OutOp   = OutOp;
optimizer.CvOp    = CvOp;
optimizer.ItUpOut = ItUpOut; 
optimizer.maxiter = maxiter;              
optimizer.m       = m;                
optimizer.run(init);

% Get results
xopt       = optimizer.xopt;
endMessage = optimizer.endingMessage;
C_values   = OutOp.evolcost;
end

