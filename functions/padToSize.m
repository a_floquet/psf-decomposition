function h = padToSize(h, sz)
% Pad h with 0 on each side to obtain a array of size sz
% I think it conserve center on both odd and even grids, but I did not
% verify
szdiff = sz - size(h);
a  = ceil(szdiff(1)/2);
b  = floor(szdiff(1)/2);
c  = ceil(szdiff(2)/2);
d  = floor(szdiff(2)/2);
h  = padarray(h, [a, c], 0, 'pre');
h  = padarray(h, [b, d], 0, 'post');
end

