function idx = placeKernels(im, H, sz)
% Interactively place each kernels on the given image
N   = size(H, 2);

fig = figure('WindowState', 'maximized');
ax  = subplot(1,2,1); imagesc(im); axis off; colormap gray;
for n = 1:N
    h = reshape(H(:, n), sz);
    subplot(1,2,2); imagesc(h); title(strcat(['Place kernel ' num2str(n) '/' num2str(N) ' on the image'])); axis off; colormap gray;
    
    % Get position
    [x, y] = ginput(1);
    idx(n, 1) = round(x);
    idx(n, 2) = round(y);
    
    hold(ax, 'on');
    scatter(ax, idx(:, 1), idx(:,2), 5, 'r', 'filled')
    text(idx(:, 1), idx(:, 2), string(1:n), 'FontSize', 10, 'Color', 'red', 'Margin', 6);
    hold(ax, 'off');
end
close(fig)
end

