function fig = plotApprox(H, A, E, sz, n)
% A is of size(sz, I), so I vectors to be reshaped into sz = (n,m) approximated kernels.
% E is vector of erros (norm of residual)
K     = size(A, 3);
[a,b] = getFigsize(K+2);

fig = figure('WindowState', 'maximized');
tiledlayout(a,b);
nexttile()
h = reshape(H, sz);
imagesc(h); axis off; colormap gray; title(strcat(['Kernel ' num2str(n)])); colormap gray; colorbar;
for k = 1:K
    nexttile()
    a_k = reshape(A(:,k), sz);
    imagesc(a_k); axis off; colormap gray; title(['Kernel ' num2str(n) ' approximation using first ' num2str(k) ' modes']); colormap gray; colorbar;
end
nexttile()
plot(1:K, E); xlabel('modes used'); ylabel('relative error');
hold on; scatter(1:K, E, 20); hold off;
xlim([1 K]); xticks(1:K);

end
