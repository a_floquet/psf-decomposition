function fig = plotError(E)
% E is (I, I) matrix of relative erros
% E_ij is the approximation error for the i-th psf using the j first modes.
N   = size(E,1);
txt = cell(N,1);
for n = 1:N
   txt{n}= sprintf('Kernel %i',n);
end

figure('WindowState', 'maximized');
plot(1:N, E(1, :)); xlabel('modes used'); ylabel('relative error');
hold on;
for n = 2:N; plot(1:N, E(n, :)); end
hold off;
legend(txt);
end

