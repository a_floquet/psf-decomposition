function fig = plotKernels(H, sz)
% H is of size(sz, I), so I vectors to be reshaped into sz = (n,m) PSF.
N     = size(H, 2);
[a,b] = getFigsize(N);
cmin  = min(H, [], 'all');
cmax  = max(H, [], 'all');

fig = figure('WindowState', 'maximized');
for n = 1:N
    nexttile()
    a_ii = reshape(H(:,n), sz);
    imagesc(a_ii); axis off; colormap gray; title(['PSF  ' num2str(n)]); colormap gray; colorbar;
end
end
