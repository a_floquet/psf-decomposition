function fig = plotModes(U, S, sz)
% U is of size(sz, I), so I vectors to be reshaped into sz = (n,m) PSF modes.
% S is vector of singular values
N     = size(U, 2);
[a,b] = getFigsize(N+1);

fig = figure('WindowState', 'maximized');
tiledlayout(a,b);
for ii = 1:N
    nexttile()
    u_n = reshape(U(:,ii), sz);
    imagesc(u_n); axis off; title(['PSF mode ' num2str(ii)]); colormap gray; colorbar;
end
nexttile()
plot(1:N, S); xlabel('mode number'); ylabel('singular values');
hold on; scatter(1:N, S, 20); hold off;
xlim([1 N]); xticks(1:N);
end

