function fig = plotWeights(W, U, sz)
% Plot modes and associated weights 
K = size(U, 2);
fig = figure('WindowState', 'maximized');
for k = 1:K
    ax(k)   = subplot(2, K, k)  ; imagesc(reshape(U(:,k), sz)); title(strcat(['Kernel ' num2str(k)])); axis off; colormap(ax(k), gray);
    ax(K+k) = subplot(2, K, K+k); imagesc(W(:,:,k))           ; title(strcat(['Weight ' num2str(k)])); axis off; colormap(ax(K+k), jet);
end

