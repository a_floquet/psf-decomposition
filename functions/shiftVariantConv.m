function [y, fig] = shiftVariantConv(x, U, W, sz)
% Apply shift variant convolution by first weighing, then convolving.
% Also display the steps
y = zeros(size(x));
K = size(U,2);
fig = figure(); 
for k = 1:K
    weighted_x = x .* W(:,:,k);
    convolved  = conv2(weighted_x, reshape(U(:,k), sz), 'same');
    y          = y + convolved;
    
    ax(k)     = subplot(5,K,k)    ; imagesc(reshape(U(:,k), sz)); axis off; colormap(ax(k), gray); 
    ax(K+k)   = subplot(5,K,K+k)  ; imagesc(W(:,:,k))           ; axis off; colormap(ax(K+k), jet);
    ax(2*K+k) = subplot(5,K,2*K+k); imagesc(weighted_x)         ; axis off; colormap(ax(2*K+k), gray);
    ax(3*K+k) = subplot(5,K,3*K+k); imagesc(convolved)          ; axis off; colormap(ax(3*K+k), gray);
end
set(ax(1),'ycolor','none')    ; ax(1).YAxis.Label.Color=[0 0 0]    ; ax(1).YAxis.Label.Visible='on'    ; ylabel(ax(1), 'modes');
set(ax(K+1),'ycolor','none')  ; ax(K+1).YAxis.Label.Color=[0 0 0]  ; ax(K+1).YAxis.Label.Visible='on'  ; ylabel(ax(K+1), 'weights');
set(ax(2*K+1),'ycolor','none'); ax(2*K+1).YAxis.Label.Color=[0 0 0]; ax(2*K+1).YAxis.Label.Visible='on'; ylabel(ax(2*K+1), 'weighted images');
set(ax(3*K+1),'ycolor','none'); ax(3*K+1).YAxis.Label.Color=[0 0 0]; ax(3*K+1).YAxis.Label.Visible='on'; ylabel(ax(3*K+1), 'convolved images');

ax(4*K+1) = subplot(5,K,4*K+1); imagesc(x); axis off; title('original image'); colormap(ax(4*K+1), gray);
ax(4*K+2) = subplot(5,K,4*K+2); imagesc(y); axis off; title('final image'); colormap(ax(4*K+2), gray);
end

