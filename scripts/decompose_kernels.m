ccc;
%% Settings
kernels = 'gaussian'; % gaussian / motion / levin
N       = 10;         % Must be 8 if kernels == 'levin'
sz      = [32 32];    % At least 27 if kernels == 'levin'
len     = 20;         % only for motion kernels, must be ok with sz
m       = 1;          % minimum gaussian variance / motion angle
M       = 10;         % maximum gaussian variance / motion angle

%% Script
% Create and fill matrix of kernels
H = generateKernels(kernels, N, m, M, sz, len);
plotKernels(H, sz);
waitforbuttonpress;

% Decompose it using the SVD
[U, S, V] = svd(H, 'econ');

% Plot basis of kernels
plotModes(U, diag(S), sz);
waitforbuttonpress;

% Approximate kernels using basis
[A,E] = computeApprox(H, U, sz);

% Plot approximation
for n = 1:N
     plotApprox(H(:, n), A(:,n,:), E(n,:), sz, n);
     waitforbuttonpress;
end

% Plot errors
plotError(E);