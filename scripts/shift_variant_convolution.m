ccc;
%% Settings
impath  = '../test_019.png';
kernels = 'gaussian'; % gaussian / motion / levin
N       = 10;         % Must be 8 if kernels == 'levin'
sz      = [32 32];    % At least 27 if kernels == 'levin'
len     = 20;         % only for motion kernels, must be ok with sz
m       = 1;          % minimum gaussian variance / motion angle
M       = 10;         % maximum gaussian variance / motion angle

%% Script
% Get and plot kernels
H = generateKernels(kernels, N, m, M, sz, len);
plotKernels(H, sz);
waitforbuttonpress;

% Get modes, plot them and choose how many modes will be used
[U, S, V] = svd(H, 'econ');
plotModes(U, diag(S), sz);
k = chooseNumberModes();

% Place each kernel on the image 
x         = double(imread(impath));
positions = placeKernels(x, H, sz);

% Compute weights for each modes & display them
weights = computeWeights(positions, H, U(:, 1:k), size(x));
plotWeights(weights, U(:, 1:k), sz);
waitforbuttonpress;

% Apply shift variant convolution
M = getGBIshiftVariantConv(U(:, 1:k), weights, size(x), sz);
y = M * x;

% Plot final results
figure('WindowState', 'maximized');
subplot(1,2,1); imagesc(x); title('original image'); axis off;
subplot(1,2,2); imagesc(y); title('convolved image'); axis off;
colormap gray