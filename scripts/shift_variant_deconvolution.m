ccc;
%% Settings
impath  = '../test_019.png';
kernels = 'gaussian'; % gaussian / motion / levin
N       = 10;         % must be 8 if kernels == 'levin'
sz      = [64 64];    % at least 27 if kernels == 'levin'
len     = 20;         % only for motion kernels, must be ok with sz
m       = 1;          % minimum gaussian variance / motion angle
M       = 5;         % maximum gaussian variance / motion angle
psnr    = 100;         % input noise PSNR in dB
lambda  = 0;          % strength of regularization (TV)
nIter   = 50;         % number of iteration for VMLMB

%% Script
% Get and plot kernels
H = generateKernels(kernels, N, m, M, sz, len);
plotKernels(H, sz);
waitforbuttonpress;

% Get modes, plot them and choose how many modes will be used
[U, S, V] = svd(H, 'econ');
plotModes(U, diag(S), sz);
k = chooseNumberModes();

% Place each kernel on the image 
x         = double(imread(impath));
positions = placeKernels(x, H, sz);

% Apply shift variant convolution & noise
weights = computeWeights(positions, H, U(:, 1:k), size(x));
M       = getGBIshiftVariantConv(U(:, 1:k), weights, size(x), sz);
y       = M * x;
y       = y + randn(size(y)) * 255 * 10^-(psnr/20); % 255 because x is in [0,255]

% Deconvolution
D = CostL2([], y) * M; 
R = CostHyperBolic([size(x) 2], 1e-5, 3) * LinOpGrad(size(x), [1 2], 'mirror');
C = D + lambda * R;
[x_est, loss, endMessage] = optimWithVMLMB(C, x, zeros(size(x)), 2, nIter, 5);

% Plot final results
figure('WindowState', 'maximized');
subplot(2,2,1); imagesc(x); title('original image'); axis off;
subplot(2,2,2); imagesc(y); title('convolved image'); axis off;
subplot(2,2,3); imagesc(x_est); title('reconstructed image'); axis off;
subplot(2,2,4); plot(1:2:nIter, loss); xlabel('iterations'); ylabel('cost function'); title(endMessage);
colormap gray